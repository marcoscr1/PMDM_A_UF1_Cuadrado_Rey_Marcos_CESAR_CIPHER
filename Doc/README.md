# Cesar Cipher

![Title](img/header.png)

**Título de la aplicación:** Cesar Cipher

**Creado por:** Marcos Cuadrado Rey

**Versión del documento:** 1.0

## Historial de versiones

| **Versión** | **Fecha** | **Comentarios** |
| --- | --- | --- |
| 1.0 |10/05/2024 | Primera versión del documento |

## Presentación

### Título de la aplicación

**Cesar Cipher**

### Concepto
**Cesar Cipher** es una aplicación para encriptar/desencriptar texto utilizando el cifrado **César**.

En el funcionamiento de este cifrado, simplemente se debe buscar cada letra de la línea del texto original y escribir la letra correspondiente en la línea codificada. Para decodificarlo se debe hacer lo contrario


### Navegación

```mermaid
graph LR

A[InboxFragment] --> B[Menu]
B --> C[EncriptedFragment]
C --> D((Codificación))
D --> C

B --> E[EncriptedFragment]
E --> F((Decodificación))
F --> E

B --> G[AboutFragment]
B --> H[HelpFragment]
```


#### InboxFragment

![InboxFragmen](img/Inbox.png)

Este fragmento se utiliza para la presentación de la aplicación, cada vez que salgamos de algún otro fragmento, volveremos a éste.

#### Menu

![Menu](img/menu.png)

Cuando pulsamos en la *toolbar* el icono de tres barras (menú), se nos abrirá un menú con las siguientes opciones:

- **Cifrar**: Cifrar texto.
- **Descifrar**: Descifrar texto.
- **Sobre app**: Datos sobre el desarrollador.
- **Ayuda**: Ayuda.

#### Cifrar

![Cifrar](img/cifrar.png)

Este fragmento se utilizará para poder cifrar el texto. Para ello hay que cubrir dos campos:

- **Desplazamiento**: Spinner donde escogeremos el desplazamiento para la encriptación de texto
- **Texto a encriptar**: Escribiremos el texto a cifrar

Una vez cubierto los campos, pulsaremos el botón **Cifrar** y aparecerá el texto cifrado en el mismo fragmento.

![Texto cifrado](img/cifrado.png)


#### Descifrar

![Cifrar](img/cifrar.png)

Este fragmento se utilizará para poder descifrar el texto. Para ello hay que cubrir dos campos:

- **Desplazamiento**: Spinner donde escogeremos el desplazamiento para la encriptación de texto.
- **Texto a desencriptar**: Escribiremos el texto cifrado.

Una vez cubierto los campos, pulsaremos el botón **Desencriptar** y aparecerá el texto plano en el mismo fragmento.

![Texto cifrado](img/descifrado.png)


#### Sobre app

![About](img/about.png)

En este fragmento aparecerán los datos del desarrollador.


#### Ayuda

![Help](img/help.png)

Este fragmento hay una breve descripción de lo que es el **cifrado César**.

