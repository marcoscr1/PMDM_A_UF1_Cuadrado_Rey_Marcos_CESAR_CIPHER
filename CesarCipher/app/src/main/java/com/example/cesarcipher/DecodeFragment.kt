package com.example.cesarcipher

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Spinner
import android.widget.TextView


class DecodeFragment : Fragment() {
    private lateinit var displacement: Spinner
    private lateinit var textEncrypted: EditText
    private lateinit var resultDecryptLayout: LinearLayout
    private lateinit var textViewResult: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_decode, container, false)

        // Referencias
        displacement = view.findViewById<Spinner>(R.id.displacement)
        textEncrypted = view.findViewById<EditText>(R.id.textToDecrypt)

        resultDecryptLayout = view.findViewById<LinearLayout>(R.id.result_encryption)

        val decryptBtn = view.findViewById<Button>(R.id.buttonDecrypt)
        val deleteBtn = view.findViewById<Button>(R.id.buttonDelete)

        textViewResult = view.findViewById<TextView>(R.id.textViewResult)

        // Accion boton encriptar
        decryptBtn.setOnClickListener {
            val displacementText = displacement.selectedItem.toString()
            val textPlaneText = textEncrypted.text.toString()

            if (displacementText.isNotEmpty() && textPlaneText.isNotEmpty()) {
                val displacementInt = displacementText.toInt()
                textViewResult.text = decode(displacementInt, textPlaneText)
            }
        }

        // Accion boton borrar
        deleteBtn.setOnClickListener {
            delete()
        }

        return view
    }

    private fun decode(displacement: Int, cipherText: String): String {
        resultDecryptLayout.visibility = View.VISIBLE
        resultDecryptLayout.clearFocus()

        return cipherText.map {
            if(it.isLetter())
                it.uppercaseChar().code.minus('A'.code).minus(displacement).plus(26).mod(26).plus('A'.code).toChar()
            else
                it
        }.joinToString(separator = "")
    }

    private fun delete() {
        displacement.setSelection(0)
        textEncrypted.setText("")
        resultDecryptLayout.visibility = View.GONE
    }
}