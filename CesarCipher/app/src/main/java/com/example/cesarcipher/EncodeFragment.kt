package com.example.cesarcipher

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Spinner
import android.widget.TextView


class EncodeFragment : Fragment() {
    private lateinit var displacement: Spinner
    private lateinit var textPlane: EditText
    private lateinit var resultEncryptionLayout: LinearLayout
    private lateinit var textViewResultado: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_encode, container, false)

        // Referencias
        displacement = view.findViewById<Spinner>(R.id.displacement)
        textPlane = view.findViewById<EditText>(R.id.textToEncrypt)

        resultEncryptionLayout = view.findViewById<LinearLayout>(R.id.result_encryption)

        val encryptBtn = view.findViewById<Button>(R.id.buttonEncrypt)
        val deleteBtn = view.findViewById<Button>(R.id.buttonDelete)

        textViewResultado = view.findViewById<TextView>(R.id.textViewResult)

        // Accion boton encriptar
        encryptBtn.setOnClickListener {
            val displacementText = displacement.selectedItem.toString()
            val textPlaneText = textPlane.text.toString()

            if (displacementText.isNotEmpty() && textPlaneText.isNotEmpty()) {
                val displacementInt = displacementText.toInt()
                textViewResultado.text = encode(displacementInt, textPlaneText)
            }
        }

        // Accion boton borrar
        deleteBtn.setOnClickListener {
            delete()
        }

        return view
    }

    private fun encode(displacement: Int, planeText: String): String {
        resultEncryptionLayout.visibility = View.VISIBLE
        resultEncryptionLayout.clearFocus()

        return planeText.map {
            if(it.isLetter())
                it.uppercaseChar().code.minus('A'.code).plus(displacement).mod(26).plus('A'.code).toChar()
            else
                it
        }.joinToString(separator = "")
    }

    private fun delete() {
        displacement.setSelection(0)
        textPlane.setText("")
        resultEncryptionLayout.visibility = View.GONE
    }
}